from django.shortcuts import render
from seatarrange import get_name_list
def Home(request):
    name_list = sorted(['gaurav','bijay','kabita','nissem','surya','suraj','sulav','navin','dinesh','dipak','pragya','shreewatsa'])
    candidates = get_name_list(name_list)
    return render(request, "seatArrangement.html", {"candidates":candidates})