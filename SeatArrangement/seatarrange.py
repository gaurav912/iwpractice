import random

def get_name_list(candidates=None):
    if not candidates:
        return None
    num = len(candidates)
    places = [i+1 for i in range(num)]
    seatMapping={}
    while places:
        selectedPlace = random.choice(places)
        selectedCandidate = random.choice(candidates)
        seatMapping.update({selectedPlace:selectedCandidate})
        places.remove(selectedPlace)
        candidates.remove(selectedCandidate)
        # print('{} : {}'.format(selectedPlace,selectedCandidate))

    # for place,candidate in sorted(seatMapping.items()):
    #     print('       {}  :  {}'.format(place,candidate))
    seatMapping = dict(sorted(seatMapping.items()))
    print(seatMapping)
    return seatMapping